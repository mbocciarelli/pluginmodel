package fr.ytopia.pluginname.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryGUI implements Listener {
    private final Inventory inv;
    private String nameInventory;

    public InventoryGUI(String name, int slot) {
    	this.nameInventory = name;
    	
        inv = Bukkit.createInventory(null, slot, nameInventory);
    }

    public void addItem(int place, ItemStack item) {
        if(place > -1 && place < inv.getSize()) {
        	inv.setItem(place, item);
        } else {
        	inv.addItem(item);
        }
    }

    // You can open the inventory with this
    public void openInventory(final HumanEntity ent) {
        ent.openInventory(inv);
    }

    // Check for clicks on items
    @EventHandler
    public void onInventoryClick(final InventoryClickEvent e) {
        
    	if(!(e.getWhoClicked() instanceof Player)) return;
    	if (!e.getInventory().equals(inv)) return;

        e.setCancelled(true);

        final ItemStack clickedItem = e.getCurrentItem();

        // verify current item is not null
        if (clickedItem == null) return;
        if(!clickedItem.hasItemMeta()) return;

        final Player p = (Player) e.getWhoClicked();

        // Using slots click is a best option for your inventory click's
        p.sendMessage("You clicked at slot " + e.getRawSlot());
    }

    // Cancel dragging in our inventory
    @EventHandler
    public void onInventoryClick(final InventoryDragEvent e) {
        if (e.getInventory().equals(inv)) {
          e.setCancelled(true);
        }
    }
}